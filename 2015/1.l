# 2024-12-01 David Pflug
# I have no idea what happened to my original solution.

(de solve-1 (Str)
   (let (L (chop Str) Pos 0)
      (while (pop 'L)
         (case @
            ("(" (inc 'Pos 1))
            (")" (dec 'Pos 1)) ) )
      (prinl Pos) ) )

(de solve-2 (Str)
   (let (L (chop Str) Pos 0 Step 0)
      (for El L
         (inc 'Step 1)
         (when (= El "(") (inc 'Pos 1))
         (when (= El ")") (dec 'Pos 1))
         (T (< Pos 0) (prinl Step)) ) ) )

(test 0 (solve-1 "(())"))
(test 0 (solve-1 "()()"))
(test 3 (solve-1 "((("))
(test 3 (solve-1 "(()(()("))
(test 3 (solve-1 "))((((("))
(test -1 (solve-1 "())"))
(test -1 (solve-1 "))("))
(test -3 (solve-1 ")))"))
(test -3 (solve-1 ")())())"))
(test 1 (solve-2 ")"))
(test 5 (solve-2 "()())"))
