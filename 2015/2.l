# 2024-12-01 David Pflug
# No idea where the original solutions went

(de wrap-gift (L W H)
   (let (Top (* L W) Face (* W H) Side (* H L))
      (+  # Wrapping a gift requires a sheet
         (* 2 (+ Top Face Side))  # The size of the surface area of the box
         (min ~(list 'Top 'Face 'Side))) ) )  # Plus the area of the smallest side

(de parse-line (Line)
   (mapcar format (mapcar pack (split Line "x"))) )

(de solve-1 (Filename)
   (let (Sum 0)
      (in Filename
         (until (eof)
            (let ((L W H) (parse-line (line)))
               (inc 'Sum (wrap-gift L W H)) ) ) )
      (prinl Sum) ) )

(de ribbon-gift (L W H)
   (use (Min Min2)
      (setq Min (min ~(list 'L 'W 'H)))
      (setq Min2 (- (+ L W H) Min (max ~(list 'L 'W 'H))))
      (+ (* 2 (+ Min Min2))  # ribbon-wrap
         (* L W H) ) ) )  # Bow

(de solve-2 (Filename)
   (let (Sum 0)
      (in Filename
         (until (eof)
            (let ((L W H) (parse-line (line)))
               (inc 'Sum (ribbon-gift L W H)) ) ) )
      (prinl Sum) ) )

(test 58 (wrap-gift 2 3 4))
(test 43 (wrap-gift 1 1 10))
(test 34 (ribbon-gift 2 3 4))
(test 14 (ribbon-gift 1 1 10))
