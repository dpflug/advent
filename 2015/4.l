# 2024-12-01 David Pflug

(de mine (Salt)
   (setq I 1)
   (loop
      (setq Str (pack Salt I))
      (setq M
         (native "libcrypto.so" "MD5" '(B . 16) Str (length Str) '(NIL (16))) )
      (inc 'I)
      (T (and (= 0 (car M) (cadr M))
            (< (caddr M) 16) )
         (prinl Str " -> " M) ) ) )
