# 2024-12-02 David Pflug

(de check-levels (Lvs)
   (let (Lo (if (< (car Lvs) (cadr Lvs)) -3 1)
         Hi (if (< (car Lvs) (cadr Lvs)) -1 3))
      (recur (Lvs)
         (cond ((not (cdr Lvs)) T)  # End of the line
            ((<= Lo (- (car Lvs) (cadr Lvs)) Hi)
               (recurse (cdr Lvs))]

(de dampened-check (Lvs Skip)
   (cond
      ((> Skip (length Lvs)) NIL)
      ((check-levels (conc (head (- Skip 1) Lvs)
               (tail (- (length Lvs) Skip) Lvs)))
         @)
      (T (dampened-check Lvs (+ 1 Skip)))]

(de parse-input (Filename)
   (in Filename
      (make (until (eof)
            (link (mapcar format (mapcar pack (split (line) " ")))]

(de solve-1 (Filename)
   (sum '((Ln) (and (check-levels Ln) 1))
      (parse-input Filename)]

(de solve-2 (Filename)
   (sum '((Report) (and (dampened-check Report 1) 1))
      (parse-input Filename)]


(test T   (check-levels   (7 6 4 2 1)))
(test NIL (check-levels   (1 2 7 8 9)))
(test NIL (check-levels   (9 7 6 2 1)))
(test NIL (check-levels   (1 3 2 4 5)))
(test NIL (check-levels   (8 6 4 4 1)))
(test T   (check-levels   (1 3 6 7 9)))
(test T   (dampened-check (7 6 4 2 1) 1))
(test NIL (dampened-check (1 2 7 8 9) 1))
(test NIL (dampened-check (9 7 6 2 1) 1))
(test T   (dampened-check (1 3 2 4 5) 1))
(test T   (dampened-check (8 6 4 4 1) 1))
(test T   (dampened-check (1 3 6 7 9) 1))
