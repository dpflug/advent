(de midpoint (L)
   (get L (inc (/ (length L) 2))) )

(de ord-sort (N M)
   (cond
      ((member (cons N M) Ords) T)
      ((member (cons M N) Ords) NIL)
      (T (< (index N Fail) (index M Fail)))]

(de solve-1 (Filename)
   (let (Ords NIL
         Upds NIL
         Failures NIL
         Sum 0
         FixSum 0)
      (in Filename
         (while (line)
            (let (L (mapcar pack (split @ "|")))
               (push 'Ords (cons (car L) (cadr L))) ) )
         (until (eof)
            (push 'Upds (mapcar pack (split (line) ","))) ) )
      (pretty Ords)
      (prinl)
      (for Upd Upds
         (catch 'fail
            (for Ord Ords
               (let (Ld (car Ord) Flw (cdr Ord))
                  (when (and (member Ld Upd)
                        (member Flw Upd)
                        (> (index Ld Upd) (index Flw Upd)) )
                     (push 'Failures Upd)
                     (throw 'fail) ) ) )
            (inc 'Sum (format (midpoint Upd))) ) )
      (pretty Failures)
      (prinl)
      (for Fail Failures
         (sort Fail ord-sort)
         (inc 'FixSum (format (midpoint Fail))) )
      (pretty Failures)
      (prinl)
      (cons Sum FixSum) ) )

#(test (143 . 123) (solve-1 "5_sample.txt"))
