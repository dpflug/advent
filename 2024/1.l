# 2024-12-01 David Pflug

(de solve-1 (Filename)
   (use (Chief Elves)
      (in Filename
         (until (eof)
            (push 'Chief (read))
            (push 'Elves (read)) ) )
      (sort Chief)
      (sort Elves)
      (prinl (sum '((N M) (abs (- N M))) Chief Elves)) ) )

(de solve-2 (Filename)
   (use (Chief Elves Counts)
      (in Filename
         (until (eof)
            (push 'Chief (read))
            (let (Elf (read))
               (or (inc (prop 'Counts Elf)) (put 'Counts Elf 1)) ) ) )
      (prinl (sum '((C) (* C (get 'Counts C))) Chief)) ) )

(test 11 (solve-1 "1_sample.txt"))
(test 31 (solve-2 "1_sample.txt"))
