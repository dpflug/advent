(de parser (Chars Do)
   # This was recursive, but I kept blowing the stack
   (let (Sum 0
         Mul1 0
         Coll NIL)
      (loop
         (NIL Chars (prinl "Final sum: " Sum))
         (state '(start)
            (start (case Next
                  ("m" 'm)
                  ("d" (if Do 'maybe-dont 'scanning))
                  (T 'scanning) )
               # 'start pulls double duty as a reset on parsing failure
               (setq Coll NIL
                  Mul1 0)
               (setq Next (pop 'Chars)) )
            (scanning (cond ((= "m" Next) 'm)
                  ((and Do (= "d" Next)) 'maybe-dont)
                  (T 'scanning) )
               (setq Next (pop 'Chars)) )
            (maybe-dont (if (not (head (chop "on't()") (cons Next Chars)))
                  'scanning
                  (do 5 (shift 'Chars)) 'dont)
               (setq Next (pop 'Chars)) )
            (dont (if (= "d" Next) 'mayhaps-do 'dont)
               (setq Next (pop 'Chars)) )
            (mayhaps-do (if (not (head (chop "o()") (cons Next Chars)))
                  'dont
                  (do 2 (shift 'Chars)) 'start)
               (setq Next (pop 'Chars)) )
            (m (if (not (head (chop "ul(") (cons Next Chars)))
                  'scanning
                  (do 2 (shift 'Chars)) 'mul)
               (setq Next (pop 'Chars)) )
            (mul (if (format Next) 'mul1 'scanning)
               Next)  # Don't reset Next as mul1 needs it
            (mul1 (cond ((= "," Next) 'comma)
                  ((format Next) 'mul1)
                  (T 'start) )
               (unless (= "," Next)
                  (push 'Coll Next) )
               (setq Next (pop 'Chars)) )
            (comma (if (format Next) 'mul2 'start)
               (setq Mul1 (format (pack (flip Coll)))
                  Coll NIL )
               Next)  # Don't reset Next as mul2 needs it
            (mul2 (cond ((= ")" Next) 'start)
                  ((format Next) 'mul2)
                  (T 'start) )
               (if (not (= ")" Next))
                  (push 'Coll Next)
                  (inc 'Sum (* Mul1 (format (pack (flip Coll))))) )
               (setq Next (pop 'Chars))]

(de solve-1 (Filename)
   (parser (in Filename (till NIL NIL)))]

(de solve-2 (Filename)
   (parser (in Filename (till NIL NIL)) T)]

(test 161 (parser (chop "xmul(2,4)%&mul[3,7]!@\^do_not_mul(5,5)+mul(32,64]then(mul(11,8)mul(8,5))")))
(test 48 (parser (chop "xmul(2,4)&mul[3,7]!\^don't()_mul(5,5)+mul(32,64](mul(11,8)undo()?mul(8,5))") T))
