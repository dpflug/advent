(de solve-1 (Filename)
   (let (Pzl NIL
         Found 0)
      (in Filename
         (until (eof)
            (push 'Pzl (line)) ) )
      (word-search)
      Found]

(de word-search ()
   (for (Y . Row) Pzl
      (for (X . Cell) Row
         (when (= Cell "X")
            (check-around X Y)]

(de check-around (X Y)
   (for DX (range -1 1)
      (for DY (range -1 1)
         (and (<= 1 (+ Y (* 3 DY)) (length Pzl))
            (<= 1 (+ X (* 3 DX)) (length (car Pzl)))
            (mas? X Y DX DY)]

(de mas? (X Y DX DY)
   (and (= "M" (get Pzl (+ Y DY) (+ X DX)))
      (= "A" (get Pzl (+ Y (* 2 DY)) (+ X (* 2 DX))))
      (= "S" (get Pzl (+ Y (* 3 DY)) (+ X (* 3 DX))))
      (inc 'Found)]

(de solve-2 (Filename)
   (let (Pzl NIL
         Found 0)
      (in Filename
         (until (eof)
            (push 'Pzl (line)) ) )
      (x-mas-search)
      Found]

(de x-mas-search ()
   (for (Y . Row) Pzl
      (for (X . Cell) Row
         (when (= Cell "M")
            (x-around X Y)]

(de x-around (X Y)
   (for DX (1 -1)
      (for DY (1 -1)
         (and (<= 1 (+ Y (* 2 DY)) (length Pzl))
            (<= 1 (+ X (* 3 DX)) (length (car Pzl)))
            (as? X Y DX DY)
            (or (as? (+ 2 X) Y (* -1 DX) DY)
               (as? X (+ 2 Y) DX (* -1 DY))
               (inc 'Found)]

(de as? (X Y DX DY)
   (and (= "A" (get Pzl (+ Y (* 2 DY)) (+ X (* 2 DX))))
      (= "S" (get Pzl (+ Y (* 2 DY)) (+ X (* 2 DX))))]

(test 18 (solve-1 "4_sample.txt"))
(test 9 (solve-2 "4_sample.txt"))
