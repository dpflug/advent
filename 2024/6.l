(de drawOnFloor (Floor DrawWait)
   # Tsk tsk, you shouldn't draw on the floor
   (put Floor GY GX "o")
   (when (and (<= 1 (+ GY DY) (length Floor))
         (<= 1 (+ GX DX) (length (car Floor))) )
      (put Floor (+ GY DY) (+ GX DX) (car Guard)) )  # Guard is a free var here, as are GX, GY, DX, and DY
                                                     # TODO: Fold drawOnFloor back into the solve
                                                     #   and use it instead of the visit list
   (let (Out NIL)
      (for (Y . Row) Floor
         (push 'Out "\n")
         (for (X . Cell) Row
            (push 'Out Cell) ) )
      (prinl (mapcar flip (flip Out)))  # RAAAAAAH FLIP OUT
      (wait DrawWait) ]

(de solve-1 (Filename DrawWait Guard)
   (default DrawWait NIL)  # By default, don't draw
   (let (Guard NIL  # Will use circ list to encode Guard behavior
         GX 0 GY 0
         DX 0 DY 0
         Visited NIL
         Floor NIL)
      (in Filename (until (eof) (queue 'Floor (line))))
      (for (Y . Row) Floor
         (for (X . Cell) Row
            (when (= "\^" Cell)
               (setq Guard '("\^" "<" "v" ">" .) GX X GY Y) ) ) )
      (println Guard)
      (while (and (<= 1 GY (length Floor))
            (<= 1 GX (length (car Floor))) )
         (push1 'Visited (cons GX GY))
         (case (car Guard)
            ("\^" (setq DX 0 DY -1))
            (">" (setq DX 1 DY 0))
            ("v" (setq DX 0 DY 1))
            ("<" (setq DX -1 DY 0)) )
         (if (and (<= 1 (+ GY DY) (length Floor))
               (<= 1 (+ GX DX) (length (car Floor)))
               (= "#" (get Floor (+ GY DY) (+ GX DX))) )
            (rot Guard)
            (when DrawWait (drawOnFloor Floor DrawWait))
            (setq GX (+ GX DX) GY (+ GY DY)) ) )
      (prinl (length Visited)) ]

#(test 41 (solve-1 "6_sample.txt"))
#(test 6 (solve-2 "6_sample.txt"))
