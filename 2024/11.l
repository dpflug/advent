(de blink (L)
   (make
      (for N L
         (cond
            ((= 0 N) (link 1))
            ((= 0 (% (length N) 2))
               (let (LogCut (** 10 (/ (length N) 2)))
                  (link (/ N LogCut)
                     (% N LogCut) ) ) )
            (T (link (* N 2024))) ]

(de solve-1 (Filename)
   (let (L (mapcar format (split (in Filename (line)) " ")))
      (length (do 25 (setq L (blink L))))]

(de solve-2 (Filename)
   (let (L (mapcar format (split (in Filename (line)) " ")))
      (length (do 75 (setq L (blink L))))]

(let (L (125 17))
   (test (253000 1 7) (setq L (blink L)))
   (test (253 0 2024 14168) (setq L (blink L)))
   (test (512072 1 20 24 28676032) (setq L (blink L)))
   (test (512 72 2024 2 0 2 4 2867 6032) (setq L (blink L)))
   (test (1036288 7 2 20 24 4048 1 4048 8096 28 67 60 32) (setq L (blink L)))
   (test (2097446912 14168 4048 2 0 2 4 40 48
         2024 40 48 80 96 2 8 6 7 6 0 3 2) (setq L (blink L)))
   (test 55312 (length (do 19 (setq L (blink L))))) )
