# 2024-12-01 David Pflug

(de wander (Str)
   (let (X 0 Y 0
         Facing 0
         Dirs (mapcar pack (split Str "," " ")) )
      (rid 'Dirs NIL)
      (for Dir Dirs
         (let (Turn (car (chop Dir))
               Vel (format (pack (cdr (chop Dir)))) )
            (case Turn
               ("R" (setq Facing (% (+ Facing 1) 4)))
               ("L" (setq Facing (- Facing 1)))
               (T (quit "Unknown direction: " Turn)) )
            (while (< Facing 0)
               (setq Facing (+ 4 Facing)))
            (case Facing
               (0 (inc 'Y Vel))
               (1 (inc 'X Vel))
               (2 (dec 'Y Vel))
               (3 (dec 'X Vel))
               (T (quit "Unknown facing: " Facing)) ) ) )
      (+ (abs X) (abs Y))]

# 496 too high
# 86 Wrong
# 3 Wrong
(de solve-1 (Filename)
   (in Filename
      (wander (line))]

(test 5 (wander (chop "R2, L3")))
(test 2 (wander (chop "R2, R2, R2")))
(test 12 (wander (chop "R5, L5, R5, R3")))
